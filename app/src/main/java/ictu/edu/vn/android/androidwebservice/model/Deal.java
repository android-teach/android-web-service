package ictu.edu.vn.android.androidwebservice.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Deal {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("note")
    private String note;
    @SerializedName("price")
    private double price;
    @SerializedName("date")
    private String date;
    @SerializedName("group")
    private int groupId;

    public Deal() {
    }

    public Deal(int id, String name, String note, double price, String date, int groupId) {
        this.id = id;
        this.name = name;
        this.note = note;
        this.price = price;
        this.date = date;
        this.groupId = groupId;
    }

    public int getId() {
        return id;
    }

    public Deal setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Deal setName(String name) {
        this.name = name;
        return this;
    }

    public String getNote() {
        return note;
    }

    public Deal setNote(String note) {
        this.note = note;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Deal setPrice(double price) {
        this.price = price;
        return this;
    }

    public String getDate() {
        return date;
    }

    public Deal setDate(String date) {
        this.date = date;
        return this;
    }

    public int getGroupId() {
        return groupId;
    }

    public Deal setGroupId(int groupId) {
        this.groupId = groupId;
        return this;
    }
}
