package ictu.edu.vn.android.androidwebservice.api;

import ictu.edu.vn.android.androidwebservice.model.DealListReponse;
import ictu.edu.vn.android.androidwebservice.model.UserReponse;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class Api {
    private static Retrofit retrofit;
    private static final String BASE_URL = "http://teach.cachhoc.net/ictu/android/";


    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public interface ApiInterface {
        @GET("deal.php?action=get")
        Call<DealListReponse> getDealList(
                @Query("token") String token,
                @Query("month") int month,
                @Query("year") int year
        );

        @FormUrlEncoded
        @POST("user.php?action=register")
        Call<UserReponse> register(
                @Field("username") String username,
                @Field("password") String password
        );

    }
}

