package ictu.edu.vn.android.androidwebservice;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ictu.edu.vn.android.androidwebservice.model.Deal;

public class DealAdapter extends RecyclerView.Adapter<DealAdapter.ViewHolder> {
    private Context context;
    private List<Deal> dealList;

    public DealAdapter(Context context, List<Deal> dealList) {
        this.context = context;
        this.dealList = dealList;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.deal_item, viewGroup, false);
        return new ViewHolder(itemView);
    }

    public int getItemCount() {
        return dealList.size();
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        // get deal item from list at position
        Deal deal = dealList.get(position);

        // display info to item view holder
        viewHolder.tvName.setText(deal.getName());
        viewHolder.tvDescription.setText(deal.getNote());
        viewHolder.tvPrice.setText(String.valueOf(deal.getPrice()));
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvDescription;
        TextView tvPrice;
        ImageView ivThumbnail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
        }
    }
}

