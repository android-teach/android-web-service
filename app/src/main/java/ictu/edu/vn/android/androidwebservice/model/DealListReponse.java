package ictu.edu.vn.android.androidwebservice.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DealListReponse {
    @SerializedName("status")
    private boolean status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private List<Deal> dealList;

    public boolean isStatus() {
        return status;
    }

    public DealListReponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public DealListReponse setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public List<Deal> getDealList() {
        return dealList;
    }

    public DealListReponse setDealList(List<Deal> dealList) {
        this.dealList = dealList;
        return this;
    }
}

