package ictu.edu.vn.android.androidwebservice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ictu.edu.vn.android.androidwebservice.api.Api;
import ictu.edu.vn.android.androidwebservice.model.Deal;
import ictu.edu.vn.android.androidwebservice.model.DealListReponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DealMonthFragment extends Fragment {
    public static final String KEY_MONTH = "month";
    public static final String KEY_YEAR = "year";

    public static DealMonthFragment newInstance(int month, int year) {
        DealMonthFragment fragment = new DealMonthFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_MONTH, month);
        args.putInt(KEY_YEAR, year);
        fragment.setArguments(args);
        return fragment;
    }

    private int month = 1;
    private int year = 2018;

    private DealAdapter adapter;
    private List<Deal> dealList = new ArrayList<>();
    private SwipeRefreshLayout refreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        month = getArguments().getInt(KEY_MONTH);
        year = getArguments().getInt(KEY_YEAR);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.deal_month, vg, false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView rvDeal = view.findViewById(R.id.rvDeal);
        adapter = new DealAdapter(getContext(), dealList);
        rvDeal.setAdapter(adapter);
        rvDeal.setLayoutManager(layoutManager);

        refreshLayout = view.findViewById(R.id.refreshLayout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        loadData();
        return view;
    }

    private void loadData() {
        refreshLayout.setRefreshing(true);

        Api.getRetrofitInstance().create(Api.ApiInterface.class)
                .getDealList("example", month, year)
                .enqueue(new Callback<DealListReponse>() {
                    @Override
                    public void onResponse(Call<DealListReponse> call, Response<DealListReponse> response) {
                        dealList.clear();
                        if (response.body().getDealList() != null) {
                            dealList.addAll(response.body().getDealList());
                        }
                        adapter.notifyDataSetChanged();
                        finishLoadData(response.body().getMsg());
                    }

                    @Override
                    public void onFailure(Call<DealListReponse> call, Throwable t) {
                        t.printStackTrace();
                        finishLoadData(t.getMessage());
                    }
                });
    }

    private void finishLoadData(String msg) {
        refreshLayout.setRefreshing(false);
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
