package ictu.edu.vn.android.androidwebservice;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.gson.Gson;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ictu.edu.vn.android.androidwebservice.api.Api;
import ictu.edu.vn.android.androidwebservice.model.UserReponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("12 - 2018");
        showFragment(this, R.id.layoutFragment, DealMonthFragment.newInstance(12, 2018), false);
    }

    private void register() {
        Api.getRetrofitInstance().create(Api.ApiInterface.class)
                .register("quan2", getMD5EncryptedString("quan1"))
                .enqueue(new Callback<UserReponse>() {
                    @Override
                    public void onResponse(Call<UserReponse> call, Response<UserReponse> response) {
                        Log.e("User", new Gson().toJson(response.body()));
                    }

                    @Override
                    public void onFailure(Call<UserReponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    public static void showFragment(AppCompatActivity activity, int layoutId,
                                    Fragment fragment, boolean isBack) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.replace(layoutId, fragment);
        if (isBack) ft.addToBackStack(null);
        ft.commit();
    }

    public static String getMD5EncryptedString(String encTarget) {
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while (md5.length() < 32) {
            md5 = "0" + md5;
        }
        return md5;
    }
}
