package ictu.edu.vn.android.androidwebservice.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserReponse {
    @SerializedName("status")
    private boolean status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private User user;

    public boolean isStatus() {
        return status;
    }

    public UserReponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public UserReponse setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public User getUser() {
        return user;
    }

    public UserReponse setUser(User user) {
        this.user = user;
        return this;
    }
}
